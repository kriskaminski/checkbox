/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    affects,
    definition,
    pgettext,
    tripetto,
} from "tripetto";
import { Checkbox } from "./";

/** Assets */
import ICON_CHECKED from "../../assets/checked.svg";
import ICON_UNCHECKED from "../../assets/unchecked.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: Checkbox,
    icon: ICON_CHECKED,
    get label() {
        return pgettext("block:checkbox", "Checkbox state");
    },
})
export class CheckboxCondition extends ConditionBlock {
    @definition
    @affects("#name")
    readonly checked: boolean = true;

    get icon() {
        return this.checked ? ICON_CHECKED : ICON_UNCHECKED;
    }

    get name() {
        return this.checked
            ? pgettext("block:checkbox", "Checked")
            : pgettext("block:checkbox", "Not checked");
    }
}
