/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    NodeBlock,
    Slots,
    conditions,
    each,
    editor,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { CheckboxCondition } from "./condition";

/** Assets */
import ICON_CHECKED from "../../assets/checked.svg";
import ICON_UNCHECKED from "../../assets/unchecked.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    alias: "checkbox",
    version: PACKAGE_VERSION,
    icon: ICON_CHECKED,
    get label() {
        return pgettext("block:checkbox", "Checkbox (single)");
    },
})
export class Checkbox extends NodeBlock {
    checkboxSlot!: Slots.Boolean;

    @slots
    defineSlot(): void {
        this.checkboxSlot = this.slots.static({
            type: Slots.Boolean,
            reference: "checked",
            label: pgettext("block:checkbox", "Checkbox checked"),
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name(false, false);
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.options();
        this.editor.required(
            this.checkboxSlot,
            pgettext("block:checkbox", "Checkbox needs to be checked")
        );
        this.editor.visibility();
        this.editor.alias(this.checkboxSlot);
        this.editor.exportable(this.checkboxSlot);
    }

    @conditions
    defineConditions(): void {
        each(
            [
                {
                    checked: true,
                    label: pgettext("block:checkbox", "Checkbox is checked"),
                    icon: ICON_CHECKED,
                },
                {
                    checked: false,
                    label: pgettext(
                        "block:checkbox",
                        "Checkbox is not checked"
                    ),
                    icon: ICON_UNCHECKED,
                },
            ],
            (condition) => {
                this.conditions.template({
                    condition: CheckboxCondition,
                    label: condition.label,
                    icon: condition.icon,
                    props: {
                        slot: this.checkboxSlot,
                        checked: condition.checked,
                    },
                });
            }
        );
    }
}
