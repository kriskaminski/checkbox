/** Dependencies */
import {
    NodeBlock,
    Slots,
    assert,
    validator,
} from "tripetto-runner-foundation";
import "./condition";

export abstract class Checkbox extends NodeBlock {
    /** Contains the checkbox slot. */
    readonly checkboxSlot = assert(
        this.valueOf<boolean, Slots.Boolean>("checked")
    ).confirm();

    /** Contains if the checkbox is required. */
    readonly required = this.checkboxSlot.slot.required || false;

    get value() {
        return this.checkboxSlot.confirm().value;
    }

    set value(value: boolean) {
        this.checkboxSlot.value = value;
    }

    /** Toggles the checkbox. */
    toggle(): void {
        this.checkboxSlot.value = !this.checkboxSlot.value;
    }

    @validator
    validate(): boolean {
        return !this.required || this.checkboxSlot.value === true;
    }
}
